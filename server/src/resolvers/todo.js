const Todo = {
  user: (parent, args, context) => {
    return context.prisma.user.findFirst({
      where: { id: parent.userId },
    });
  },
};

module.exports = Todo;
