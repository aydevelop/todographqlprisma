const { AuthenticationError } = require("apollo-server-express");

const Query = {
  helloWorld: () => `Hello world! what a day!`,
  users: (parent, args, context, info) => {
    return context.prisma.user.findMany();
  },
  user: (parent, args, context, info) => {
    return context.prisma.user.findUnique({
      where: { id: parseInt(args.userId) },
    });
  },
  todos: async (parent, args, context, info) => {
    if (!context.isAuthenticated()) {
      throw new AuthenticationError("Must be logged in to view todos");
    }

    const user = await context.getUser();
    // return context.prisma.todo.findMany({
    //   where: { userId: parseInt(user.id) },
    // });

    let whereConditions = [
      { userId: parseInt(user.id) },
      { name: { contains: args.filter } },
    ];

    args.takeStatus === "complete"
      ? whereConditions.push({ isComplete: true })
      : null;

    args.takeStatus === "incomplete"
      ? whereConditions.push({ isComplete: false })
      : null;

    return context.prisma.todo.findMany({
      where: {
        AND: whereConditions,
      },
      orderBy: {
        id: "desc",
      },
    });

    //return context.prisma.todo.findMany();
  },
  me: async (parent, args, context, info) => {
    if (context.getUser()) {
      return context.getUser();
    }
  },
};

module.exports = Query;
