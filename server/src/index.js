const { ApolloServer } = require("apollo-server-express");
const { PrismaClient } = require("@prisma/client");
const resolvers = require("./resolvers");
const typeDefs = require("./typedefs");

const express = require("express");
const { buildContext } = require("graphql-passport");
const { passportInit } = require("./utils");

const app = express();
const prisma = new PrismaClient({ errorFormat: "minimal" });
passportInit(prisma, app);

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req, res }) => {
    return buildContext({ req, prisma });
  },
});

app.use((err, req, res, next) => {
  console.log(err.stack);
  res.status(500).send("error");
});

app.get("/", (req, res) => {
  res.redirect("/graphql");
});

const PORT = 4000;
server.applyMiddleware({
  app,
  cors: { credentials: true, origin: "http://localhost:3000" },
});
app.listen({ port: PORT }, () => {
  console.log(
    `🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`
  );
});
