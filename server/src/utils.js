const passport = require("passport");
const { GraphQLLocalStrategy } = require("graphql-passport");
const bcrypt = require("bcryptjs");
const session = require("express-session");
const { v4: uuidv4 } = require("uuid");

function createUserId() {
  let id = 0;
  function incrementId() {
    id++;
    return id;
  }
  return incrementId;
}

function passportInit(prisma, app) {
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  passport.deserializeUser((id, done) => {
    const user = prisma.user.findUnique({
      where: {
        id,
      },
    });

    done(null, user);
  });

  const SESSION_SECRET = "24jl234haosdih00v8e###dsefg34$f$";
  app.use(
    session({
      genid: (req) => uuidv4(),
      secret: SESSION_SECRET,
      resave: false,
      saveUninitialized: false,
      cookie: { maxAge: 60000 },
    })
  );

  app.use(passport.initialize());
  app.use(passport.session()); // if session is used

  passport.use(
    new GraphQLLocalStrategy(async (email, password, done) => {
      const matchingUser = await prisma.user.findUnique({
        where: {
          email: email,
        },
      });

      let error = matchingUser ? null : new Error("no matching user");
      if (matchingUser) {
        const valid = await bcrypt.compare(password, matchingUser.password);
        error = valid ? "" : new Error("Invalid password");
      }
      done(error, matchingUser);
    })
  );
}

module.exports = {
  createUserId,
  passportInit,
};
