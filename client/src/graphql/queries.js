import { gql } from "@apollo/client";

const TODOS_QUERY = gql`
  query todos(
    $f: String
    $takeStatus: String = "incomplete"
    $skip: Int = 0
    $take: Int = 1000000
  ) {
    todos(filter: $f, takeStatus: $takeStatus, skip: $skip, take: $take) {
      id
      name
      userId
      isComplete
    }
  }
`;

const ME = gql`
  query {
    me {
      id
      email
      firstName
    }
  }
`;

const LOGOUT_MUTATION = gql`
  mutation {
    logout
  }
`;

export { TODOS_QUERY, ME, LOGOUT_MUTATION };
