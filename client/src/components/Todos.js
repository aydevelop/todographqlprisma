import Reac, { useState } from "react";
import TodoItem from "./TodoItem";
import { useTodoItems } from "../hooks/todo-items";

function Todos(props) {
  const [skip, setSkip] = useState(0);
  const { data, loading } = useTodoItems({
    dashInput: props.dashInput,
    takeStatus: props.takeStatus,
  });

  if (loading) {
    return <div>Loading ...</div>;
  }

  //let todoRows = data.todoItems.map((elem) => {
  let todoRows = data?.todos?.map((elem) => {
    return (
      <TodoItem
        key={elem.id}
        id={elem.id}
        completed={elem.isComplete}
        task={elem.name}
      />
    );
  });

  return (
    <div>
      <h3>My Todos</h3>
      {todoRows}
    </div>
  );
}

export default Todos;
