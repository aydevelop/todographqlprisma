import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useQuery, useMutation, gql } from "@apollo/client";
import Todos from "../components/Todos";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import Alert from "@material-ui/lab/Alert";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { useCreateTodoItem } from "../hooks/todo-items";
import Radio from "@material-ui/core/Radio";
import { useAuth } from "../context/AuthContext";

function Dashboard() {
  const [isSearch, setIsSearch] = useState(false);
  const [takeStatus, setTakeStatus] = useState("incomplete");
  const { data, loading, error } = useQuery(TODOS_QUERY, {});
  const [dashInput, setDashInput] = useState("");
  const { createTodo, error: errorNewTodo } = useCreateTodoItem();
  const { useCurrentUser } = useAuth();

  if (loading) {
    return <div>Loading...</div>;
  }
  if (error) {
    return <div>Error...</div>;
  }

  const handleSubmit = (event) => {
    event.preventDefault();

    createTodo({
      variables: {
        newTodo: dashInput,
        userId: 1,
        isComplete: false,
      },
    });
    setDashInput("");
  };

  return (
    <Container maxWidth="sm">
      <form onSubmit={handleSubmit}>
        <TextField
          fullWidth
          id="dashInput"
          label={isSearch ? "Search Todos" : "Add a todo"}
          value={dashInput}
          onChange={(e) => setDashInput(e.target.value)}
          variant="outlined"
          margin="normal"
        />
        <FormControlLabel
          control={
            <Checkbox
              checked={isSearch}
              color="primary"
              name="searchTodos"
              onChange={() => setIsSearch(!isSearch)}
            />
          }
          label="Search Todos"
        />
        <FormControlLabel
          control={
            <Radio
              checked={takeStatus === "complete"}
              onChange={() => setTakeStatus("complete")}
              color="primary"
              name="complete"
            />
          }
          label="Complete"
        />
        <FormControlLabel
          control={
            <Radio
              checked={takeStatus !== "complete"}
              onChange={() => setTakeStatus("incomplete")}
              color="primary"
              name="complete"
            />
          }
          label="Incomplete"
        />
        {!isSearch && (
          <Button fullWidth type="submit" variant="contained" color="primary">
            Add Todo
          </Button>
        )}
        <br /> <br />
      </form>
      {errorNewTodo && <Alert severity="error">{errorNewTodo}</Alert>}
      <Box align="center">
        <Todos takeStatus={takeStatus} dashInput={isSearch ? dashInput : ""} />
      </Box>
    </Container>
  );
}

export default Dashboard;

const TODOS_QUERY = gql`
  query {
    todos {
      id
      name
      isComplete
    }
  }
`;

const NEW_TODO = gql`
  mutation createTodo($newTodo: String!, $userId: ID!, $isComplete: Boolean!) {
    createTodo(name: $newTodo, isComplete: $isComplete, userId: $userId) {
      id
      name
      isComplete
      userId
    }
  }
`;
