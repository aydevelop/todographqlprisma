import React from "react";
import { useAuth } from "../context/AuthContext";

function AuthenticatedHome() {
  const { currentUser } = useAuth();

  return (
    <h2>
      <center>Hi user - {currentUser.firstName}</center>
    </h2>
  );
}

export default AuthenticatedHome;
