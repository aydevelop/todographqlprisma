import React, { useState } from "react";
import { CardElement, useElements, useStripe } from "@stripe/react-stripe-js";
import { useSignup } from "../hooks/user";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Alert from "@material-ui/lab/Alert";
import { LoadingButton } from "@material-ui/lab";

function Signup() {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const { doSignup, error } = useSignup();

  const elements = useElements();
  const stripe = useStripe();
  const [checkoutError, setCheckoutError] = useState("");
  const [isProcessing, setProcessingTo] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();

    const billingDetails = {
      name: e.target.firstName.value,
      email: e.target.email.value,
      address: {
        city: "e.target.city.value",
        line1: "e.target.address.value",
        state: "e.target.state.value",
        postal_code: "e.target.zip.value",
      },
    };

    setProcessingTo(true);
    const cardElement = elements.getElement("card");
    const paymentMethodReq = await stripe.createPaymentMethod({
      type: "card",
      card: cardElement,
      billing_details: billingDetails,
    });

    if (paymentMethodReq.error) {
      console.log("error paymentMethod: ", paymentMethodReq);
      // error handling
      setCheckoutError(paymentMethodReq.error.message);
      setProcessingTo(false);
    } else {
      console.log("paymentMethod: ", paymentMethodReq);
      let paymentId = paymentMethodReq.paymentMethod.id;
      console.log("paymentId: " + paymentId);

      doSignup({
        variables: {
          email,
          password,
          paymentMethod: paymentId,
          firstName,
        },
      });
    }
  };

  const handleCardDetailsChange = (ev) => {
    ev.error ? setCheckoutError(ev.eror.message) : setCheckoutError();
  };

  const cardElementOpts = {
    iconStyle: "solid",
    // style: someStyles
    hidePostalCode: true,
  };

  return (
    <Container component="main" maxWidth="xs">
      <div>
        <Typography
          component="h1"
          variant="h3"
          style={{ marginTop: 80, textAlign: "center" }}
        >
          Sign Up
        </Typography>
        <form onSubmit={handleSubmit} noValidate>
          <TextField
            fullWidth
            id="firstName"
            label="First Name"
            margin="normal"
            name="firstName"
            onChange={(e) => setFirstName(e.target.value)}
            required
            value={firstName}
            variant="outlined"
          />
          <TextField
            fullWidth
            id="email"
            label="Email"
            margin="normal"
            name="email"
            onChange={(e) => setEmail(e.target.value)}
            required
            value={email}
            variant="outlined"
          />
          <TextField
            fullWidth
            id="password"
            label="Password"
            margin="normal"
            name="password"
            onChange={(e) => setPassword(e.target.value)}
            required
            value={password}
            type="password"
            variant="outlined"
          />
          <br />
          <br />
          <div
            style={{ border: "1px solid gray", padding: 15, borderRadius: 5 }}
          >
            <CardElement
              options={cardElementOpts}
              onChange={handleCardDetailsChange}
            />
          </div>
          <br />
          <br />
          {!isProcessing ? (
            <Button type="submit" fullWidth variant="contained" color="primary">
              Sign Up
            </Button>
          ) : (
            <Button
              disabled
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
            >
              Loading
            </Button>
          )}
        </form>
        {checkoutError && <Alert severity="error">{checkoutError}</Alert>}
        {error && <Alert severity="error">{error}</Alert>}
      </div>
    </Container>
  );
}

export default Signup;
