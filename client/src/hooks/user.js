import {
  LOGIN_MUTATION,
  LOGOUT_MUTATION,
  SIGNUP_MUTATION,
} from "../graphql/mutations";
import { useMutation, useQuery } from "@apollo/client";
//import { useHistory } from "react-router-dom";
import { ME } from "../graphql/queries";
import { useHistory } from "react-router-dom";

function useLoginMutation() {
  //let history = useHistory();
  const [doLogin, { error, loading }] = useMutation(LOGIN_MUTATION, {
    onCompleted() {
      console.log("useLoginMutation onCompleted!");
      //history.push("/dashboard");
      window.location.href = "/dashboard";
    },
    onError() {
      console.log("useLoginMutation error");
    },
  });

  return { doLogin, error: error?.message, loading };
}

function useCurrentUser() {
  const meQuery = useQuery(ME);

  if (meQuery.loading) {
    return { currentUser: "" };
  }

  if (meQuery.data?.me) {
    return {
      currentUser: meQuery.data.me,
      status: "complete",
    };
  } else {
    return { currentUser: "", status: "complete" };
  }
}

function useLogout() {
  let history = useHistory();
  const [doLogout, { client }] = useMutation(LOGOUT_MUTATION, {
    onCompleted() {
      client.resetStore();
      //history.push("/");
      //window.location.assign(window.location);
      window.location.href = "/";
    },
  });
  return { doLogout };
}

function useSignup() {
  let history = useHistory();
  const [doSignup, { error, client }] = useMutation(SIGNUP_MUTATION, {
    onCompleted() {
      client.resetStore();
      // history.push("/dashboard");
      // window.location.assign(window.location);
      window.location.href = "/";
    },
    onError() {
      // erros can be handled here.
    },
  });
  return { doSignup, error: error?.message };
}

export { useLoginMutation, useCurrentUser, useLogout, useSignup };
